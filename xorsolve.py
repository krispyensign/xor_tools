"""Performs Gaussian elimination in GF(2)"""
import numpy
from scipy.sparse import csr_matrix, lil_matrix

def swap_rows(mat, rowa, rowb):
    """Swap rows of sparse matrix"""
    temp = mat[rowa]
    mat[rowa] = mat[rowb]
    mat[rowb] = temp

def echelon(matrix):
    """Converts matrix to reduced row echelon form"""
    num_rows = matrix.shape[0]
    i_max = 0
    for diag in range(num_rows):
        if matrix[diag, diag] == 0:
            if matrix[diag + 1:, diag].nnz != 0:
                i_max = numpy.argmax(matrix[diag + 1:, diag]) + diag + 1
            if matrix[i_max, diag] == 1:
                swap_rows(matrix, diag, i_max)
        if matrix[diag, diag] == 1:
            for i in range(diag + 1, num_rows):
                if matrix[i, diag] == 1:
                    matrix[i] = xor_vector(matrix.getrow(i), matrix.getrow(diag))

def backsolve(matrix):
    """Performs backsolve of RREF"""
    num_rows = matrix.shape[0]
    for row in range(num_rows - 1, -1, -1):
        i_max = numpy.argmax(matrix[row, :])
        if matrix[row, i_max] == 1:
            for i in range(row - 1, -1, -1):
                if matrix[i, i_max] == 1:
                    matrix[i] = xor_vector(matrix.getrow(row), matrix.getrow(i))

def row_sort(matrix):
    """Sorts rows according to leading True element"""
    num_rows = matrix.shape[0]
    num_cols = matrix.shape[1]
    for i in range(num_rows):
        for j in range(num_rows - i - 1):
            max_rowa = numpy.argmax(matrix[j])
            max_rowb = numpy.argmax(matrix[j + 1])
            if matrix[j, max_rowa] == 0:
                max_rowa = num_cols
            if matrix[j + 1, max_rowb] == 0:
                max_rowb = num_cols
            if max_rowa > max_rowb:
                swap_rows(matrix, j, j + 1)

def decode(matrix):
    """transforms backsolve solution into free variable form"""
    num_cols = matrix.shape[1]
    out_matrix = csr_matrix(lil_matrix((num_cols, num_cols)))
    for col in range(num_cols):
        if matrix[:, col].nnz == 0:
            out_matrix[col, col] = 1
    for row in range(matrix.shape[0]):
        if matrix[row, row] == 1:
            temp_vector = matrix[row, :].transpose()
            temp_vector[row] = 0
            out_matrix[:, row] = xor_vector(temp_vector, out_matrix[:, row])
        elif matrix[row, :].nnz != 0:
            i_max = numpy.argmax(matrix[row, :])
            temp_vector = matrix[row, :]
            temp_vector[0, i_max] = 0
            out_matrix[:, i_max] = xor_vector(temp_vector.transpose(), out_matrix[:, i_max])
        else:
            out_matrix[row, row] = 1
    out_matrix = numpy.transpose(out_matrix)
    for col in range(num_cols):
        if out_matrix[col, :].nnz == 0:
            out_matrix[col, col] = 1
    return out_matrix

def xor_vector(vectora, vectorb):
    """takes two vectors and calculates element-wise mod 2"""
    temp_vector = vectora + vectorb
    temp_vector.data[:] = temp_vector.data[:] % 2
    return temp_vector

def gaussian(matrix):
    """Requires boolean csr_matrix"""
    echelon(matrix)
    backsolve(matrix)
    row_sort(matrix)
    return decode(matrix)

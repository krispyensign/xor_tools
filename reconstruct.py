"""Reconstruct solutions edges"""

# can be done ahead of time
def generate_edge_list(incident_matrix):
    """ Converts an incident matrix to a edge list """
    out_list = []
    num_cols = incident_matrix.shape[1]
    for col in range(num_cols):
        temp_col = incident_matrix.getcol(col)
        nz_indices = temp_col.nonzero()
        out_list.append(nz_indices[0].tolist())
    return out_list

def get_free_variables(solution_matrix):
    """Given matrix find free variables"""
    mapping = []
    num_rows = solution_matrix.shape[0]
    for row in range(num_rows):
        if solution_matrix.getrow(row).nnz == 1:
            mapping.append(row)
    return mapping

# Remainder of these should be executed in parallel
def recover_edges(solution_matrix, binary_code_str):
    """Given csr matrix with solution set and code, reconstruct edge set"""
    num_rows = solution_matrix.shape[0] # => variable maps
    num_cols = solution_matrix.shape[1] # => variables
    output_vector = [0] * num_rows
    solution_matrix = solution_matrix.transpose()
    # zeroize free variables according to binary_code_str recipe
    for col in range(num_cols):
        if binary_code_str[col] == '0':
            solution_matrix[col] = 0

    solution_matrix = solution_matrix.transpose()
    for row in range(num_rows):
        output_vector[row] = solution_matrix[row].getnnz() % 2
    return output_vector

def difference(list_a, list_b):
    """ Calculate difference of 2 lists """
    set_a = set(list_a)
    set_b = set(list_b)
    out_list = list(set_a - set_b)
    return out_list

def intersect(list_a, list_b):
    """ Calculate intersection of 2 lists """
    set_a = set(list_a)
    set_b = set(list_b)
    out_list = list(set_a.intersection(set_b))
    return out_list

def reconstruct_graph(edge_vector, edge_list):
    """ reconstructs a subgraph of edges by gluing random set of edges together """
    sub_a = [edge for index, edge in enumerate(edge_list) if edge_vector[index] == 1]
    graph = []
    edge = sub_a.pop()
    graph.append(edge)
    while len(sub_a) >= 1:
        for index, next_edge in enumerate(sub_a):
            set_intersect = intersect(edge, next_edge)
            if len(set_intersect) == 1:
                edge = sub_a.pop(index)
                graph.append(edge)
                break
    return graph

def edge_match(graph, quick_edges, edge_list):
    """ matches """
    new_graph = []
    for graph_edge in graph:
        for index, quick_edge in enumerate(quick_edges):
            if not difference(graph_edge, quick_edge):
                new_graph.append(edge_list[index])
                continue
    return new_graph

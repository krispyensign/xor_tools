"""Given list of strings with a delimeter create incident matrix"""
import numpy

def get_edges(filename, delimeter):
    """gets edges from file"""
    vertices_list = []
    with open(filename) as the_file:
        edges_list = the_file.read().splitlines()
    for pair in edges_list:
        vertices_list += pair.split(delimeter)
    vertices_list = list(set(vertices_list))
    vertices_dict = {}
    edges_dict = {}
    for index, vertex in enumerate(vertices_list):
        vertices_dict[vertex] = index
    for index, edge in enumerate(edges_list):
        edges_dict[edge] = index
    return edges_dict, vertices_dict, edges_list, vertices_list

def generate_incident(edges, vertices, delimeter):
    """Given list of strings with a delimeter create incident matrix"""
    out_matrix = []
    num_cols = len(edges)
    num_rows = len(vertices)
    out_matrix = numpy.zeros((num_rows, num_cols), dtype=int)
    for index, edge in enumerate(edges):
        vertex_set = edge.split(delimeter)
        for vertex in vertex_set:
            vertex_index = vertices[vertex]
            out_matrix[vertex_index][index] = 1
    return out_matrix

def edges_to_list(edges_list, vertices_list, delimeter):
    """generates quick lookup dict"""
    out_list = [None] * len(edges_list)
    for index, edge in enumerate(edges_list):
        edge_vertices = edge.split(delimeter)
        new_edge = [vertices_list.index(x) for x in edge_vertices]
        out_list[index] = new_edge
    return out_list
